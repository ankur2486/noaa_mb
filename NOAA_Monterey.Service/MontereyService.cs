﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Web.Script.Serialization;
using System.Configuration;
using NOAA_Monterey.Service.Model;
using Newtonsoft.Json;

namespace NOAA_Monterey.Service
{
    public class MontereyService : IMontereyService
    {
        private string XmlPath
        {
            get { return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Configuration"); }
        }

        private string HtmlTemplatePath
        {
            get { return ConfigurationManager.AppSettings["HtmlTemplatePath"]; }
        }

        private int weatherConfig
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["weatherConfig"]))
                    return 1;
                else
                    return Convert.ToInt32(ConfigurationManager.AppSettings["weatherConfig"]);
            }
        }

        public MontereyService()
        {

        }

        public ConfigurationModel GetConfiguration()
        {
            ConfigurationModel cModel = new ConfigurationModel();
            try
            {
                cModel.PagesText = ReadXMLFile("monterey_screentext.xml");
                cModel.WeatherDetails = ReadAllText("WeatherJson.txt");
                cModel.PagesTimeout = ReadPageTimeoutXMLFile("monterey_pagetimeout.xml");
                cModel.SearchURL = GetWhitelistURL("WhitelistUrl.txt");
                cModel.WeatherConfig = weatherConfig;
            }
            catch (Exception ex)
            {
                AppLogger.Log.Error(ex.Message, ex);
            }

            return cModel;
        }

        public RenderHTMLModel RenderHTMLContents(string pageName)
        {
            RenderHTMLModel model = new RenderHTMLModel();
            model.HTMLContents = GetHtmlPageContent(string.Format("{0}.html", pageName));
            return model;
        }

        private string GetHtmlPageContent(string fileName)
        {
            try
            {
                string htmlPage = Path.Combine(HtmlTemplatePath, fileName);
                string strHTML = System.IO.File.ReadAllText(htmlPage);
                return strHTML;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private string ReadXMLFile(string fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string path = Path.Combine(XmlPath, fileName);
                doc.Load(path);

                string json = JsonConvert.SerializeXmlNode(doc.ChildNodes[1]);
                return json;
            }
            catch (Exception ex)
            {
                AppLogger.Log.Error(ex.Message, ex);
            }
            return string.Empty;
        }

        private List<string> GetWhitelistURL(string FileName)
        {
            string path = Path.Combine(XmlPath, FileName);
            string[] lines = File.ReadAllLines(path);
            return lines.ToList();
        }
        private string ReadPageTimeoutXMLFile(string fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string path = Path.Combine(XmlPath, fileName);
                doc.Load(path);

                string json = JsonConvert.SerializeXmlNode(doc.ChildNodes[1]);
                json = json.Replace("@", "").Replace("#", "");

                return json;
            }
            catch (Exception ex)
            {
                AppLogger.Log.Error(ex.Message, ex);
            }

            return string.Empty;
        }

        private string ReadAllText(string FileName)
        {
            string jsonText = File.ReadAllText(Path.Combine(XmlPath, FileName));
            return jsonText;
        }
    }
}
