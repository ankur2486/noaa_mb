﻿using NOAA_Monterey.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace NOAA_Monterey.Service
{
    [ServiceContract]
    public interface IMontereyService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "GetConfiguration", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        ConfigurationModel GetConfiguration();

        [OperationContract]
        [WebInvoke(UriTemplate = "RenderHTMLContents?pageName={pageName}", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        RenderHTMLModel RenderHTMLContents(string pageName);
    }
}
