﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NOAA_Monterey.Service.Model
{
    [DataContract]
    public class ConfigurationModel
    {
        [DataMember]
        public string PagesText { get; set; }

        [DataMember]
        public string WeatherDetails { get; set; }

        [DataMember]
        public string PagesTimeout { get; set; }

        [DataMember]
        public List<string> SearchURL { get; set; }

        [DataMember]
        public int WeatherConfig { get; set; }

    }
}
