﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NOAA_Monterey.Service.Model
{
   public class RenderHTMLModel
    {
        [DataMember]
        public string HTMLContents { get; set; }
       
    }
}
