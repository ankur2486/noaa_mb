﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using log4net;

namespace NOAA_Monterey.Service
{
    public class AppLogger
    {
        static AppLogger()
        {
            Log = LogManager.GetLogger(typeof(AppLogger));
        }

        public static ILog Log { get; set; }

        public static void LogException(Exception ex)
        {
            Log.Error(ex.Message, ex);
        }
    }
}