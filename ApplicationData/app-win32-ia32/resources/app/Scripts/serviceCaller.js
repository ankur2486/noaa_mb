﻿var service = function (errorCallback) {
    this.callService = function (url, callback) {
        $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            cache: false,
            success: callback,
            error: function (error) {
                console.log(error);
                errorCallback(error);
            }
        });
    }

    this.callServicePost = function (url, data, callback) {
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            contentType: 'application/json',
            dataType: 'json',
            processData: false,
            cache: false,
            success: callback,
            error: function (error) {
                console.log(error);
                errorCallback(error);
            }
        });
    }
};

service.prototype.BaseUrl = 'http://localhost:8573/MontereyService';

service.prototype.getConfiguration = function (callback) {
    this.callService(this.BaseUrl + '/GetConfiguration', callback);
};

service.prototype.renderHTMLContents = function (pageName, callback) {
    this.callService(this.BaseUrl + '/RenderHTMLContents?pageName=' + pageName, callback);
};


