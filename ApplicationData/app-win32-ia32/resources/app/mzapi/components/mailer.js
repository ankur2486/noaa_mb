/**
 * Email functions that send messages via the management server.
 *
 * @namespace components.mailer
 * @memberof mzero.create
 */
mzero.create.components.mailer = (function() {
	function convertToKeyValueArray(keysValues) {
		if(!keysValues) return null;
		var result = [];
		Object.getOwnPropertyNames(keysValues).forEach(function(element,index,array) {
			result.push( { "Key" : element, "Value" : keysValues[element] } );
		});
		return result;
	};
	/** @lends mzero.create.components.mailer */
	var exports = {
		/**
		 * Send a simple email
		 * @param {string} fromAddress - Email address from which the message should come from
		 * @param {string} destinationAddress - Email address to which the message is addressed
		 * @param {string} subject - The message subject
		 * @param {string} body	- The message body
		 */
		sendSimple: function(fromAddress, destinationAddress, subject, body) {
			var self = this;
			var emailParams = {
				"a_from" : fromAddress,
				"a_to" : destinationAddress,
				"subject" : subject,
				"content" : body
			};
			var request = mzero.create.report.send("email_pool", emailParams);
			request.start();
		}
	};
	return exports;
})();
