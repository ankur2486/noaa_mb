/**
 * Interface to the Presence service provided by the platform
 *
 * @namespace presence
 * @memberof mzero.create
 */
mzero.create.presence = (function() {
	var service = mzero.create.MAPEnv.createService("Presence");
	
	function startPlatformMonitorProximityRequest(eventHandlers) {
		var request = service.createRequest("ReceiveProximityStates", { }, {TagPersists: true})
			.withSignal("end", "CancelReceiveProximityStates", 0)
			.firingEvents({ ProximityChange: "ProximityChange" })
			.succeedsOn("ReceiveProximityStatesSucceeded")
			.start(eventHandlers);
		return request;
	}

	function startPlatformMonitorUserSessionRequest(eventHandlers) {
		var request = service.createRequest("ReceiveUserSessionStates", { }, {TagPersists: true})
			.withSignal("end", "CancelReceiveUserSessionStates", 0)
			.firingEvents({ UserSessionChange: "UserSessionChange" })
			.succeedsOn("ReceiveUserSessionStatesSucceeded")
			.start(eventHandlers);
		return request;
	}
	
	/**
	 * Represents a Request to monitor user proximity directly
	 * @name MonitorProximityRequest
	 * @class
	 * @memberof mzero.create.presence
	 * @extends mzero.create.Request
	 * @see mzero.create.presence.monitorProximity
	 */
	/**
	 * Start the proximity monitoring request, providing the callback handlers to be used.
	 * <p>
	 * <b>IMPORTANT:</b> In the existing implementation, the <tt>stateChange</tt> callback might be invoked very soon, <u>even before the <tt>start()</tt> call itself returns</u>.
	 * It is therefore better to avoid modifying objects right after the <tt>start()</tt> call if they are also modified in the <tt>stateChange</tt> handler (otherwise you will
	 * find yourself debugging a non-existing problem).
	 * </p>
	 *
	 * @param eventHandlers {Object} - Event handlers
	 * @param eventHandlers.StateChange {mzero.create.EventCallback} - Invoked as soon as the request starts (to notify you of the current state) and afterwards whenever the state changes.
	 *                                                                 The <tt>data</tt> parameter will be "On" when an object is detected, "Off" when none is detected, and "Unknown" if the state has not been determined yet.
	 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished
	 * @method
	 * @name mzero.create.presence.MonitorProximityRequest#start
	 */
	/**
	 * Stops monitoring proximity
	 * @name end
	 * @method
	 * @instance
	 * @memberof mzero.create.presence.MonitorProximityRequest
	 */
	
	
	// ts begins monitorProximity
;

;

;

var ProximityState;
(function (ProximityState) {
    ProximityState[ProximityState["Unknown"] = 0] = "Unknown";
    ProximityState[ProximityState["Off"] = 1] = "Off";
    ProximityState[ProximityState["On"] = 2] = "On";
})(ProximityState || (ProximityState = {}));
;

var MonitorSetupPhase;
(function (MonitorSetupPhase) {
    MonitorSetupPhase[MonitorSetupPhase["None"] = 0] = "None";
    MonitorSetupPhase[MonitorSetupPhase["UsingExisting"] = 1] = "UsingExisting";
    MonitorSetupPhase[MonitorSetupPhase["Activating"] = 2] = "Activating";
    MonitorSetupPhase[MonitorSetupPhase["Ready"] = 3] = "Ready";
    MonitorSetupPhase[MonitorSetupPhase["Deactivated"] = 4] = "Deactivated";
})(MonitorSetupPhase || (MonitorSetupPhase = {}));
;

var MonitorProximityRequest = (function () {
    function MonitorProximityRequest() {
        this.started = false;
        this._logger = mzero.create.getLogger("MonitorProximityRequest");
        this._setupPhase = 0 /* None */;
        this._proximityState = this._previousProximityState = 0 /* Unknown */;
        this._isFirstStatusChangeEvent = true;
        this._monitor = null;
    }
    MonitorProximityRequest.prototype.start = function (eventHandlers) {
        this._eventHandlers = eventHandlers || {};
        this._monitor = monitoredProximityDevices.getInstance();
        if (this._monitor.isActive()) {
            this.prepareToUseExistingMonitor();
        } else {
            this.prepareToWaitForMonitorSetUp();
        }
        this.started = true;
    };

    MonitorProximityRequest.prototype.prepareToUseExistingMonitor = function () {
        // We only need to subscribe to its notification event
        this._setupPhase = 3 /* Ready */;
        var self = this;
        this._callbacks = {
            StateChange: function (err, data) {
                self.fireStateChange(data);
            }
        };
        this._monitor.startReceivingProximityEvents(this);
        this.fireStateChange(this._monitor.currentProximityState());
    };

    MonitorProximityRequest.prototype.prepareToWaitForMonitorSetUp = function () {
        this._setupPhase = 2 /* Activating */;
        var self = this;
        this._callbacks = {
            Connected: function (err, data) {
                self.completeSetup(err, data);
            },
            StateChange: function (err, data) {
                self.fireStateChange(data);
            }
        };
        this._monitor.startReceivingProximityEvents(this);
    };

    MonitorProximityRequest.prototype.completeSetup = function (err, data) {
        if (err) {
            this._logger.info("Global proximity monitor could not be set up because of error=" + err);
            this._setupPhase = 4 /* Deactivated */;
            this.fireFinalEvent(err, null);
            return;
        }
        this._logger.info("Global proximity monitor is now available");
        this._setupPhase = 3 /* Ready */;
        this.fireStateChange(data);
    };

    MonitorProximityRequest.prototype.fireStateChange = function (newState) {
        this._logger.debug("fireStateChange isfirst=" + this._isFirstStatusChangeEvent + " new=" + newState.toString() + " previous=" + this._previousProximityState.toString());
        if (this._isFirstStatusChangeEvent || (newState != this._proximityState)) {
            this._isFirstStatusChangeEvent = false;
            this._previousProximityState = this._proximityState;
            this._proximityState = newState;
            var eventHandlerName = "StateChange";
            var handler = this._eventHandlers[eventHandlerName];
            if (handler) {
                handler(null, newState);
            } else {
                this._logger.warn("Request has no handler for '" + eventHandlerName + "'");
            }
        }
    };

    MonitorProximityRequest.prototype.fireFinalEvent = function (err, data) {
        var handler = this._eventHandlers["Done"];
        if (handler) {
            handler(err, data);
        }
    };

    MonitorProximityRequest.prototype.end = function () {
        if (this._setupPhase == 0 /* None */) {
            this._logger.warn("Ignoring stopMonitor on request that has never been started");
            return;
        }
        if (this._setupPhase == 4 /* Deactivated */) {
            this._logger.warn("Ignoring stopMonitor on deactivated request");
            return;
        }
        var phase = this._setupPhase;
        this._setupPhase = 4 /* Deactivated */;

        if (this._monitor) {
            this._monitor.stopReceivingProximityEvents(this);
        }
        if (phase == 2 /* Activating */) {
            this.fireFinalEvent("Cancelled", null);
        } else {
            this.fireFinalEvent(null, null);
        }
    };
    return MonitorProximityRequest;
})();

var MonitoredProximityDeviceSet = (function () {
    function MonitoredProximityDeviceSet() {
        this._instance = null;
    }
    MonitoredProximityDeviceSet.prototype.getInstance = function () {
        if (this._instance == null) {
            this._instance = new SharedProximityMonitor();
        }
        return this._instance;
    };

    MonitoredProximityDeviceSet.prototype.removeMonitor = function () {
        this._instance = null; // so that a future request retries the MAPLink connection from a clean slate (this will deactivate any bad callback we may have acquired)
    };
    return MonitoredProximityDeviceSet;
})();

var monitoredProximityDevices = new MonitoredProximityDeviceSet();

/**
* The MAP service Presence service dispatches events to all interested parties but the messages have
* to flow through the STOMP broker on each notification. It is more efficient to provide a common
* entry at the API level
*/
var SharedProximityMonitor = (function () {
    /**
    * @private
    */
    function SharedProximityMonitor() {
        this._previousProximityState = 0 /* Unknown */;
        this._currentProximityState = 0 /* Unknown */;
        this._objectsToCallBack = [];
        this._logger = mzero.create.getLogger("SharedProximityMonitor");
    }
    SharedProximityMonitor.prototype.isActive = function () {
        return (this._monitorProximityRequest != null) && this._isSetUp;
    };

    // This accessor is intended to be used only when initializing new MonitorProximityRequest objects
    SharedProximityMonitor.prototype.currentProximityState = function () {
        return this._currentProximityState;
    };

    SharedProximityMonitor.prototype.startReceivingProximityEvents = function (request) {
        this._objectsToCallBack.push(request);
        if (this._monitorProximityRequest == null) {
            // If no request already in progress, start a new one
            this.requestMapProximityStates();
        }
    };

    SharedProximityMonitor.prototype.stopReceivingProximityEvents = function (request) {
        var found = false;
        for (var i = 0; !found && (i < this._objectsToCallBack.length); i++) {
            found = (request === this._objectsToCallBack[i]);
        }
        if (found) {
            this._objectsToCallBack = this._objectsToCallBack.splice(i, 1);
        }
    };

    SharedProximityMonitor.prototype.requestMapProximityStates = function () {
        this._isSetUp = false;
        var self = this;
        this._monitorProximityRequest = startPlatformMonitorProximityRequest({
            ProximityChange: function (err, data) {
                var state = data.State;
                if (self.isValidAndNew(state)) {
                    self._logger.debug("New proximity state=" + state);
                    self.fireStatusChange(state);
                }
            },
            Done: function (err, data) {
                if (err) {
                    self.completeFailedSetUp(err);
                } else {
                    self.completeSuccessfulSetUp(data);
                }
            }
        });
    };

    SharedProximityMonitor.prototype.isValidAndNew = function (proximityState) {
        return (proximityState != null) && (proximityState != this._currentProximityState);
    };

    SharedProximityMonitor.prototype.fireStatusChange = function (proximityState) {
        this._previousProximityState = this._currentProximityState;
        this._currentProximityState = proximityState;
        this._invokeCallbacks("StateChange", null, this._currentProximityState);
    };

    SharedProximityMonitor.prototype.completeFailedSetUp = function (err) {
        this._logger.warn("Global proximity monitor events not set up because of error=" + err);
        if (this._isSetUp) {
            this._logger.error("Assumption violation: failed call after successful set up");
        }
        this._invokeCallbacks("Connected", err, null);
        this._monitorProximityRequest = null;
        this._isSetUp = false;
        monitoredProximityDevices.removeMonitor(); // so that a future request retries the MAPLink connection from a clean slate (this will deactivate any bad listeners we may have acquired)
    };

    SharedProximityMonitor.prototype.completeSuccessfulSetUp = function (data) {
        this._logger.info("Global proximity monitor is ready");
        if (this._isSetUp) {
            this._logger.error("Assumption violation: more success on a successfully set up monitor");
        }
        this._isSetUp = true;
		var state = data['State'];
		this.fireStatusChange(state); // dispatching to sharing listeners
    };

    SharedProximityMonitor.prototype._invokeCallbacks = function (callbackType, err, data) {
        for (var i = 0; i < this._objectsToCallBack.length; i++) {
			var obj = this._objectsToCallBack[i];
			var callback = obj._callbacks[callbackType];
			if (callback) {
				callback(err, data);
			}
        }
    };
    return SharedProximityMonitor;
})();

	// ts ends monitorProximity
	
	// ts begins monitorUserSession
;

;

;

var UserSessionState;
(function (UserSessionState) {
    UserSessionState[UserSessionState["Unknown"] = 0] = "Unknown";

    /// <summary>
    /// Generated whenever there are no active user sessions on the machine
    /// </summary>
    UserSessionState[UserSessionState["NoSession"] = 1] = "NoSession";

    /// <summary>
    /// Generated when a session starts
    /// </summary>
    UserSessionState[UserSessionState["SessionStarted"] = 2] = "SessionStarted";

    /// <summary>
    /// Generated when a session is idle waiting for user activity
    /// </summary>
    UserSessionState[UserSessionState["SessionIdle"] = 3] = "SessionIdle";

    /// <summary>
    /// Generated when a session resumes after being idle
    /// </summary>
    UserSessionState[UserSessionState["SessionResumed"] = 4] = "SessionResumed";

    /// <summary>
    /// Generated when a session finishes
    /// </summary>
    UserSessionState[UserSessionState["SessionFinished"] = 5] = "SessionFinished";
})(UserSessionState || (UserSessionState = {}));
;

var MonitorSetupPhase;
(function (MonitorSetupPhase) {
    MonitorSetupPhase[MonitorSetupPhase["None"] = 0] = "None";
    MonitorSetupPhase[MonitorSetupPhase["UsingExisting"] = 1] = "UsingExisting";
    MonitorSetupPhase[MonitorSetupPhase["Activating"] = 2] = "Activating";
    MonitorSetupPhase[MonitorSetupPhase["Ready"] = 3] = "Ready";
    MonitorSetupPhase[MonitorSetupPhase["Deactivated"] = 4] = "Deactivated";
})(MonitorSetupPhase || (MonitorSetupPhase = {}));
;

var MonitorUserSessionRequest = (function () {
    function MonitorUserSessionRequest() {
        this.started = false;
        this._logger = mzero.create.getLogger("MonitorUserSessionRequest");
        this._setupPhase = 0 /* None */;
        this._userSessionState = this._previousUserSessionState = 0 /* Unknown */;
        this._isFirstStatusChangeEvent = true;
        this._monitor = null;
    }
    MonitorUserSessionRequest.prototype.start = function (eventHandlers) {
        this._eventHandlers = eventHandlers || {};
        this._monitor = monitoredUserSessionDevices.getInstance();
        if (this._monitor.isActive()) {
            this.prepareToUseExistingMonitor();
        } else {
            this.prepareToWaitForMonitorSetUp();
        }
        this.started = true;
    };

    MonitorUserSessionRequest.prototype.prepareToUseExistingMonitor = function () {
        // We only need to subscribe to its notification event
        this._setupPhase = 3 /* Ready */;
        var self = this;
        this._callbacks = {
            StateChange: function (err, data) {
                self.fireStateChange(data);
            }
        };
        this._monitor.startReceivingUserSessionEvents(this);
        this.fireStateChange(this._monitor.currentUserSessionState());
    };

    MonitorUserSessionRequest.prototype.prepareToWaitForMonitorSetUp = function () {
        this._setupPhase = 2 /* Activating */;
        var self = this;
        this._callbacks = {
            Connected: function (err, data) {
                self.completeSetup(err, data);
            },
            StateChange: function (err, data) {
                self.fireStateChange(data);
            }
        };
        this._monitor.startReceivingUserSessionEvents(this);
    };

    MonitorUserSessionRequest.prototype.completeSetup = function (err, data) {
        if (err) {
            this._logger.info("Global userSession monitor could not be set up because of error=" + err);
            this._setupPhase = 4 /* Deactivated */;
            this.fireFinalEvent(err, null);
            return;
        }
        this._logger.info("Global userSession monitor is now available");
        this._setupPhase = 3 /* Ready */;
        this.fireStateChange(data);
    };

    MonitorUserSessionRequest.prototype.fireStateChange = function (newState) {
        this._logger.debug("fireStateChange isfirst=" + this._isFirstStatusChangeEvent + " new=" + newState.toString() + " previous=" + this._previousUserSessionState.toString());
        if (this._isFirstStatusChangeEvent || (newState != this._userSessionState)) {
            this._isFirstStatusChangeEvent = false;
            this._previousUserSessionState = this._userSessionState;
            this._userSessionState = newState;
            var eventHandlerName = "StateChange";
            var handler = this._eventHandlers[eventHandlerName];
            if (handler) {
                handler(null, newState);
            } else {
                this._logger.warn("Request has no handler for '" + eventHandlerName + "'");
            }
        }
    };

    MonitorUserSessionRequest.prototype.fireFinalEvent = function (err, data) {
        var handler = this._eventHandlers["Done"];
        if (handler) {
            handler(err, data);
        }
    };

    MonitorUserSessionRequest.prototype.end = function () {
        if (this._setupPhase == 0 /* None */) {
            this._logger.warn("Ignoring stopMonitor on request that has never been started");
            return;
        }
        if (this._setupPhase == 4 /* Deactivated */) {
            this._logger.warn("Ignoring stopMonitor on deactivated request");
            return;
        }
        var phase = this._setupPhase;
        this._setupPhase = 4 /* Deactivated */;

        if (this._monitor) {
            this._monitor.stopReceivingUserSessionEvents(this);
        }
        if (phase == 2 /* Activating */) {
            this.fireFinalEvent("Cancelled", null);
        } else {
            this.fireFinalEvent(null, null);
        }
    };
    return MonitorUserSessionRequest;
})();

var MonitoredUserSessionDeviceSet = (function () {
    function MonitoredUserSessionDeviceSet() {
        this._instance = null;
    }
    MonitoredUserSessionDeviceSet.prototype.getInstance = function () {
        if (this._instance == null) {
            this._instance = new SharedUserSessionMonitor();
        }
        return this._instance;
    };

    MonitoredUserSessionDeviceSet.prototype.removeMonitor = function () {
        this._instance = null; // so that a future request retries the MAPLink connection from a clean slate (this will deactivate any bad callback we may have acquired)
    };
    return MonitoredUserSessionDeviceSet;
})();

var monitoredUserSessionDevices = new MonitoredUserSessionDeviceSet();

/**
* The MAP service Presence service dispatches events to all interested parties but the messages have
* to flow through the STOMP broker on each notification. It is more efficient to provide a common
* entry at the API level
*/
var SharedUserSessionMonitor = (function () {
    /**
    * @private
    */
    function SharedUserSessionMonitor() {
        this._previousUserSessionState = 0 /* Unknown */;
        this._currentUserSessionState = 0 /* Unknown */;
        this._objectsToCallBack = [];
        this._logger = mzero.create.getLogger("SharedUserSessionMonitor");
    }
    SharedUserSessionMonitor.prototype.isActive = function () {
        return (this._monitorUserSessionRequest != null) && this._isSetUp;
    };

    // This accessor is intended to be used only when initializing new MonitorUserSessionRequest objects
    SharedUserSessionMonitor.prototype.currentUserSessionState = function () {
        return this._currentUserSessionState;
    };

    SharedUserSessionMonitor.prototype.startReceivingUserSessionEvents = function (request) {
        this._objectsToCallBack.push(request);
        if (this._monitorUserSessionRequest == null) {
            // If no request already in progress, start a new one
            this.requestMapUserSessionStates();
        }
    };

    SharedUserSessionMonitor.prototype.stopReceivingUserSessionEvents = function (request) {
        var found = false;
        for (var i = 0; !found && (i < this._objectsToCallBack.length); i++) {
            found = (request === this._objectsToCallBack[i]);
        }
        if (found) {
            this._objectsToCallBack = this._objectsToCallBack.splice(i, 1);
        }
    };

    SharedUserSessionMonitor.prototype.requestMapUserSessionStates = function () {
        this._isSetUp = false;
        var self = this;
        this._monitorUserSessionRequest = startPlatformMonitorUserSessionRequest({
            UserSessionChange: function (err, data) {
                var state = data.State;
                if (self.isValidAndNew(state)) {
                    self._logger.debug("New userSession state=" + state);
                    self.fireStatusChange(state, data);
                }
            },
            Done: function (err, data) {
                if (err) {
                    self.completeFailedSetUp(err);
                } else {
                    self.completeSuccessfulSetUp(data);
                }
            }
        });
    };

    SharedUserSessionMonitor.prototype.isValidAndNew = function (userSessionState) {
        return (userSessionState != null) && (userSessionState != this._currentUserSessionState);
    };

    SharedUserSessionMonitor.prototype.fireStatusChange = function (userSessionState, data) {
        this._previousUserSessionState = this._currentUserSessionState;
        this._currentUserSessionState = userSessionState;
        this._invokeCallbacks("StateChange", null, { State: this._currentUserSessionState, SessionId: data.SessionId });
    };

    SharedUserSessionMonitor.prototype.completeFailedSetUp = function (err) {
        this._logger.warn("Global userSession monitor events not set up because of error=" + err);
        if (this._isSetUp) {
            this._logger.error("Assumption violation: failed call after successful set up");
        }
        this._invokeCallbacks("Connected", err, null);
        this._monitorUserSessionRequest = null;
        this._isSetUp = false;
        monitoredUserSessionDevices.removeMonitor(); // so that a future request retries the MAPLink connection from a clean slate (this will deactivate any bad listeners we may have acquired)
    };

    SharedUserSessionMonitor.prototype.completeSuccessfulSetUp = function (data) {
        this._logger.info("Global userSession monitor is ready");
        if (this._isSetUp) {
            this._logger.error("Assumption violation: more success on a successfully set up monitor");
        }
        this._isSetUp = true;
		var state = data['State'];
		this.fireStatusChange(state, data); // dispatching to sharing listeners
    };

    SharedUserSessionMonitor.prototype._invokeCallbacks = function (callbackType, err, data) {
        for (var i = 0; i < this._objectsToCallBack.length; i++) {
			var obj = this._objectsToCallBack[i];
			var callback = obj._callbacks[callbackType];
			if (callback) {
				callback(err, data);
			}
        }
    };
    return SharedUserSessionMonitor;
})();
	
	// ts ends monitorUserSession

	/** @lends mzero.create.presence */
	var exports = {
		/**
		 * Monitor user proximity directly
		 * @return {mzero.create.presence.MonitorProximityRequest} A request ready to be started
		 */
		monitorProximity: function() {
			var result = new MonitorProximityRequest();
			return result;
		},
		_startSession: function(sessionId) {
			var request = service.createRequest("StartUserSession", { SessionId: sessionId })
				.succeedsOn("StartUserSessionSucceeded")
			;
			return request;
		},
		_finishSession: function() {
			var request = service.createRequest("FinishUserSession", { })
				.succeedsOn("FinishUserSessionSucceeded")
			;
			return request;
		},
		/**
		 * Monitor changes in user session states
		 */
		monitorUserSession: function() {
			var result = new MonitorUserSessionRequest();
			return result;
		}
	};
	return exports;
})();
