﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using NOAA_Monterey.Service;


namespace NOAA_Monterey.HostService
{
    public partial class MonetereyHostService : ServiceBase
    {
        ServiceHost host = null;

        public MonetereyHostService()
        {
            InitializeComponent();              
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                if (host != null)
                    host.Close();

                host = new ServiceHost(typeof(NOAA_Monterey.Service.MontereyService));
                host.Open();             
            }
            catch (Exception ex)
            {
                AppLogger.LogException(ex);
            }
        }

        protected override void OnStop()
        {
            host.Close();
        } 
    }
}
