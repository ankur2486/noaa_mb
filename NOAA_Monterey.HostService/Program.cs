﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace NOAA_Monterey.HostService
{
    static class Program
    {
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new MonetereyHostService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
