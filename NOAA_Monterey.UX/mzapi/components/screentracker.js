/**
 * Components to track screen entries and exits, transparently reporting complete records to the management server
 * @namespace components.screentracker
 * @memberof mzero.create
 */
mzero.create.components.screentracker = (function() {
	function SingleTimer(milliseconds, callback)
	{
		this._milliseconds = milliseconds;
		this._callback = callback;
		delete this._timeoutId;
	}
	SingleTimer.prototype.start = function() {
		var self = this;
		this._timeoutId = setTimeout(this._milliseconds, function() {
			delete this._timeoutId;
			self._callback();
		});
	};
	SingleTimer.prototype.stop = function() {
		if(typeof this._timeoutId == "number") {
			clearTimeout(this._timeoutId);
			delete this._timeoutId;
		}
	};
	SingleTimer.prototype.reset = function() {
		this.stop();
		this.start();
	};

	/**
	 * Information on a screen report
	 */
	function ScreenRecord(sourceName/*String*/, sessionId/*String*/, screenName/*String*/, enterTime/*Date*/, enterReason/*String*/) 
	{
		this.source = sourceName;
		this.sessionId = sessionId;
		this.name = screenName;
		this.enterTime = enterTime;
		this.enterReason = enterReason;
		this.exitTime = null;
		this.exitReason = null;
	}
	
	ScreenRecord.prototype.clone = function()/*ScreenRecord*/
	{
		var result = new ScreenRecord(this.source, this.sessionId, this.name, this.enterTime, this.enterReason);
		result.exitTime = this.exitTime;
		result.exitReason = this.exitReason;
		return result;
	}
	
	ScreenRecord.prototype.markRedisplayed = function(sessionId/*String*/, redisplayTime/*Date*/, redisplayReason/*String*/)/*void*/
	{
		this.sessionId = sessionId; // I don't think it makes sense to change the session ids during this operation, but we're being permissive so far...
		this.enterTime = redisplayTime;
		this.enterReason = redisplayReason;
		this.exitTime = null;
		this.exitReason = null;
	}
	
	ScreenRecord.prototype.exit = function(exitTime/*Date*/, exitReason/*String*/)/*void*/
	{
		this.exitTime = exitTime;
		this.exitReason = exitReason;
	}

	function calculateDurationInSeconds(record/*ScreenRecord*/)/*String*/
	{
		if (record.exitTime == null) {
			return null;
		}
		var result/*Number*/ = (record.exitTime.getTime() - record.enterTime.getTime()) / 1000.0;
		return result.toFixed(3);
	}
	
	var ScreenReportName = "custom_screens";
	var MaxBackLogSize = 200;
	
	function _ScreenReporter() 
	{
		this._logger = mzero.create.getLogger("_ScreenReporter");
		this._backLog /*Array<ScreenRecord>*/ = [];
		this._currentRecord /*ScreenRecord*/ = null;
		this._retryTimer/*SingleTimer*/;
		this._shuttingDown = false;
		this._setupTimer();
		// TODO - we might need to hook a before unload event, either in jquery http://stackoverflow.com/a/11440300/1623757
		// or in window.onbeforeunload. The decision belows to the programmer so this should be an external function
		/*
		if (RuntimeIdentification.isDesktopPlayer()) {
			NativeApplication.nativeApplication.addEventListener(Event.EXITING, onApplicationExiting);
		}
		*/
	}

	_ScreenReporter.prototype._onApplicationExiting = function(e/*Event*/)/*void*/ 
	{
		// TODO: Any non-reported screens should actually be persisted  :-/
		this._shuttingDown = true;
		this._logger.info("Preparing for shutdown, backlog size=" + this._backLog.length);
		this._retryTimer.stop();
	}
	
	_ScreenReporter.prototype._setupTimer = function()/*void*/ 
	{
		var self = this;
		this._retryTimer = new SingleTimer(ReportRetryIntervalInSeconds * 1000, function() {
			self._logger.info("Time to retry screen report");
			self._retryTimer.reset();
			self._sendCurrentRecord();
		});
		// The timer will only be started when it's needed
	}
	
	_ScreenReporter.prototype.startSending = function(screenRecord/*ScreenRecord*/)/*void*/ 
	{
		if (this._currentRecord != null) {
			this._queueForSendingLater(screenRecord);
		} else {
			this._startSendingRecordNow(screenRecord);
		}
	}
	
	_ScreenReporter.prototype._queueForSendingLater = function(screenRecord/*ScreenRecord*/)/*void*/ 
	{
		if (this._backLog.length >= MaxBackLogSize) {
			this._logger.info("Maximum backlog reached, dropping oldest record");
			this._backLog.shift();
		}
		this._backLog.push(screenRecord.clone()); // Need to make a copy in case the screen is redisplayed
		this._logger.info("Queued record for sending when possible");
	}
	
	_ScreenReporter.prototype._startSendingRecordNow = function(screenRecord/*ScreenRecord*/)/*void*/ 
	{
		if (this._currentRecord != null) {
			this._logger.warn("Will overwrite current screen name=" + this._currentRecord.name + " with new one=" + screenRecord.name);
		}
		this._currentRecord = screenRecord;
		this._sendCurrentRecord();
	}
	
	// Send the information currently in this._currentRecord
	_ScreenReporter.prototype._sendCurrentRecord = function()/*void*/ 
	{
		if (this._currentRecord == null) {
			this._logger.error("Cannot send null screen record");
			return;
		}
		if (!mzero.create.MAPEnv.isInitialized) {
			this._logger.info("MAPEnv not initialized yet; will retry later");
			this._scheduleRetry();
			return;
		}
		var self = this;
		var screenReport  = this._createScreenReport(this._currentRecord);
		var eventHandlers = {
			Done: function(err, data) {
				self._onSendReportAttempted(err, data);
			}
		};
		var request/*SendReportRequest*/ = mzero.create.report.send(ScreenReportName, screenReport).start(eventHandlers);
	}
	
	_ScreenReporter.prototype._createScreenReport = function(record/*ScreenRecord*/)/*Object*/
	{
		var now = new Date();
		var result/*Object*/ = { };
		result["source"] = record.source;
		result["session_id"] = record.sessionId;
		result["begin_time"] = mzero.create.report._formatLocalTimestamp(record.enterTime);
		result["end_time"] = mzero.create.report._formatLocalTimestamp(record.exitTime);
		result["duration"] = calculateDurationInSeconds(record);
		result["exit_reason"] = record.exitReason;
		result["screen_name"] = record.name;
		result["report_time"] = mzero.create.report._formatLocalTimestamp(now);
		result["custom_id_1"] = mzero.create.MAPEnv.AppData.MachineId1; // so it always uses the latest value
		return result;
	}
	
	_ScreenReporter.prototype._onSendReportAttempted = function(err, data)/*void*/ 
	{
		if (this._shuttingDown) {
			this._logger.info("Got report result during shutdown: success=" + e.success + "; this will be the last one");
			return;
		}
		var backLogSize/*int*/ = this._backLog.length;
		if (err) {
			this._logger.info("Report could not be sent now because of error=" + err + " current backlog size is " + backLogSize + "; will retry");
			this._scheduleRetry();
		} else {
			this._currentRecord = null;
			this._logger.debug("Send succeeded; backlog size is " + backLogSize);
			if (backLogSize != 0) {
				this._startSendingRecordNow(this._backLog.shift());
			}
		}
	}
	
	_ScreenReporter.prototype._scheduleRetry = function()/*void*/
	{
		this._retryTimer.start();
	}
	
	var ReportRetryIntervalInSeconds = 2.0;
	// These special screens (and reason) will be inserted automatically during reporting when they're needed
	var ReportingStartScreenName = "Startup";
	var ReportingStartReason = "start";
	var ReportingEndScreenName = "Shutdown";
	var SessionStartScreenName = "Session start";
	var SessionExitScreenName = "Session exit";
	
	/**
	 * Synchronous component that keeps track of screens being entered and exited within a single display or ("source area") whose name can be provided on initialization.
	 * When a screen is exited, its record is considered complete and then it is sent to the management server.
	 * @class mzero.create.components.screentracker.ScreenTracker
	 */
	function ScreenTracker(options) {
		this._logger = mzero.create.getLogger("ScreenTracker");
		options = options || { };
		this._sourceName = options.Display || "main";
		this._sessionId = null;
		this._reporter = new _ScreenReporter();
		this._screenStack/*ScreenRecord*/ = [];
		this._enterApplicationStartScreen();
	}
	
	/**
	 * Use this to indicate that screen reporting is stopping, most likely because the application is shutting down
	 * @param {String} reason - Reason why the application is shutting down
	 * @method mzero.create.components.screentracker.ScreenTracker#shutdown
	 */
	ScreenTracker.prototype.shutdown = function(reason) {
		this.popDownAndEnter(ReportingEndScreenName, reason);
	}
	
	
	ScreenTracker.prototype._enterApplicationStartScreen = function() {
		this.enter(ReportingStartScreenName, ReportingStartReason);
	}
	
	ScreenTracker.prototype._logScreenStack = function() {
		var screenStructureString = this._screenStack.map(this._extractScreenName, this).join(" :: ");
		this._logger.debug("Screen structure: [ " + screenStructureString + "]");
	}
	
	ScreenTracker.prototype._extractScreenName = function(element,index,array) {
		return element.name;
	}
	
	/**
	 * Report that a new session starts and the given screen is being entered
	 * 
	 * @param {String} sessionId - Unique identifier to be used to report the session
	 * @param {String} screenName - Name of the screen where this session starts
	 * @param {String} reason - Reason why the screen is entered (which implicitly becomes the reason why the previous screen exited)
	 * @method mzero.create.components.screentracker.ScreenTracker#enterSession
	 */
	ScreenTracker.prototype.enterSession = function(sessionId/*String*/, screenName/*String*/, reason/*String*/) {
		var now = new Date();
		this._sessionId = sessionId;
		if(this._sessionId != null) {
			this._enterAt(now, SessionStartScreenName, reason);
			mzero.create.presence._startSession(sessionId).start({});
		} else {
			this._logger.info("Sessions are not started with null ids");
		}
		this._enterAt(now, screenName, reason);
		this._logScreenStack();
	}
	
	/**
	 * Report that the current session ends and the given screen is being entered
	 *
	 * @param {String} screenName - Name of the screen being entered (which is now outside of the usage session)
	 * @param {String} reason - Reason why the screen is entered (which implicitly becomes the reason why the previous screen exited)
	 * @method mzero.create.components.screentracker.ScreenTracker#exitSession
	 */
	ScreenTracker.prototype.exitSession = function(screenName/*String*/, reason/*String*/)/*void*/
	{
		var now = new Date();
		if (this._sessionId != null) {
			// A session was active
			this._enterAt(now, SessionExitScreenName, reason);
			mzero.create.presence._finishSession().start({});
			this._sessionId = null;
		}
		this._enterAt(now, screenName, reason);
		this._logScreenStack();
	}
	
	/**
	 * Indicate that a screen is starting because of the specified reason. Implicitly, this reason
	 * becomes the exit reason for the previous screen.
	 * 
	 * @param {String} screenName - Name of the screen being entered
	 * @param {String} reason - Reason why the screen is entered (which implicitly becomes the reason why the previous screen exited)
	 * @method mzero.create.components.screentracker.ScreenTracker#enter
	 */
	ScreenTracker.prototype.enter = function(screenName/*String*/, reason/*String*/)/*void*/
	{
		var now = new Date();
		this._enterAt(now, screenName, reason);
		this._logScreenStack();
	}
	
	ScreenTracker.prototype._enterAt = function(date, screenName, reason)/*void*/
	{
		var newScreen = new ScreenRecord(this._sourceName, this._sessionId, screenName, date, reason);
		var contextIndex/*int*/ = this._getContextIndexAtTop();
		
		if (contextIndex < 0) {
			this._logger.info("Entering in new context: screen=" + screenName + "; reason=" + reason);
			this._screenStack.push(newScreen);
		} else {
			var previousScreen = this._screenStack[contextIndex];
			previousScreen.exit(date, reason);
			this._screenStack[contextIndex] = newScreen;
			this._reporter.startSending(previousScreen);
			this._logger.info("Exiting screen=" + previousScreen.name + "; reason=" + reason + "; new screen=" + screenName);
		}
	}
	
	ScreenTracker.prototype._getContextIndexAtTop = function()/*int*/
	{
		return this._screenStack.length - 1;
	}
	
	/**
	 * Indicate that a popup is now being displayed over the current screen; future calls to
	 * enter() will refer to the popup itself until popDown() is called (at which
	 * point, the screen being displayed before calling popUp() will resume being the
	 * current screen).
	 * 
	 * @param {String} screenName - Name of the pop up
	 * @param {String} reason - Reason why the pop up is entered (which implicitly becomes the reason why the previous screen exited)
	 * @method mzero.create.components.screentracker.ScreenTracker#popUp
	 */
	ScreenTracker.prototype.popUp = function(screenName/*String*/, reason/*String*/)/*void*/
	{
		var now = new Date();
		this._popUpAt(now, screenName, reason);
		this._logScreenStack();
	}
	
	ScreenTracker.prototype._popUpAt = function(date, screenName/*String*/, reason/*String*/)/*void*/
	{
		var popUpScreen = new ScreenRecord(this._sourceName, this._sessionId, screenName, date, reason);
		
		var contextIndex/*int*/ = this._getContextIndexAtTop();
		if (contextIndex < 0) {
			this._screenStack.push(popUpScreen); // TODO: (tbd) not sure if it is a good idea to start the context with a popup
			this._logger.warn("Screen popped up without entering any screens first; name=" + screenName + " reason=" + reason);
			return;
		}
		// Complete the current screen record, then send it
		var previousScreen = this._screenStack[contextIndex];
		previousScreen.exit(date, reason);
		this._reporter.startSending(previousScreen);
		this._screenStack.push(popUpScreen);
		this._logger.info("Pop up screen name=" + screenName + " over=" + previousScreen.name + " reason=" + reason);
	}
	
	/**
	 * Indicate that the current pop up is being exited, and the previous screen
	 * is now being displayed.
	 * 
	 * @param {String} reason - Reason why the pop up exited (which implicitly becomes the reason why the overlayed screen is redisplayed)
	 * @method mzero.create.components.screentracker.ScreenTracker#popDown
	 */
	ScreenTracker.prototype.popDown = function(reason/*String*/)/*void*/
	{
		var now = new Date();
		this._popDownAt(now, reason);
		this._logScreenStack();
	}
	
	ScreenTracker.prototype._popDownAt = function(date, reason/*String*/)/*void*/
	{
		var contextIndex/*int*/ = this._getContextIndexAtTop();
		if (contextIndex < 0) {
			this._logger.warn("popUp called without entering any screens first");
			return;
		}
		var popUpScreen/*ScreenRecord*/ = this._screenStack.pop();
		popUpScreen.exit(date, reason);
		this._reporter.startSending(popUpScreen);
		
		contextIndex = this._getContextIndexAtTop();
		if (contextIndex < 0) {
			this._logger.warn("No nested screen context left");
			return;
		}
		var currentScreen/*ScreenRecord*/ = this._screenStack[contextIndex];
		currentScreen.markRedisplayed(this._sessionId, date, reason);
		this._logger.info("Pop down into screen name=" + currentScreen.name + " from=" + popUpScreen.name + " reason=" + reason);
	}
	
	/**
	 * "Pop down" out of all the current popups and enter the specified screen
	 * @param {String} screenName - Name of the screen being entered
	 * @param {String} reason - Reason why the pop ups are being exited (which implicitly becomes the reason why the specified screen is now being entered)
	 * @method mzero.create.components.screentracker.ScreenTracker#popDownAndEnter
	 */
	ScreenTracker.prototype.popDownAndEnter = function(screenName/*String*/, reason/*String*/)/*void*/
	{
		var now = new Date();
		this._popDownAndEnterAt(now, screenName, reason);
		this._logScreenStack();
	}
	
	ScreenTracker.prototype._popDownAndEnterAt = function(date, screenName/*String*/, reason/*String*/)/*void*/
	{
		var contextIndex/*int*/ = this._getContextIndexAtTop();
		while (contextIndex > 0) 
		{
			this._popDownAt(date, reason);
			--contextIndex;
		}
		this._enterAt(date, screenName, reason);
	}
	
	/**
	 * @return The partial record corresponding to the current screen (if any).
	 * @method mzero.create.screentracker.ScreenTracker#currentScreen
	 */
	ScreenTracker.prototype.currentScreen = function()/*ScreenRecord*/
	{
		var contextIndex/*int*/ = this._getContextIndexAtTop();
		if (contextIndex >= 0) {
			return this._screenStack[contextIndex];
		} else {
			return null;
		}
	}
	
	/** @lends mzero.create.components.screentracker */
	var exports = {
		/**
		 * Create a screen tracker instance, which can then be used to report screen screentracker on your application
		 *
		 * @param options - Screen tracker options; can be null
		 * @param options.Display - An optional identifier for the display source on which the screens are being tracked; if not specified, <tt>main</tt> will be used
		 * @return {mzero.create.components.screentracker.ScreenTracker} A <tt>ScreenTracker</tt> object customized for the specified options
		 */
		"create": function(options) {
			return new ScreenTracker(options);
		}
	}
	return exports;
})();
