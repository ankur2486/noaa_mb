/**
 * Interface to the System service provided by the platform,
 * which allows some control about the platform life cycle
 * and reports information about the state of its modules.
 *
 * @namespace system
 * @memberof mzero.create
 */
mzero.create.system = (function() {
	
	var service = mzero.create.MAPEnv.createService("System");

	/**
	 * A generic property of a system module
	 * @class mzero.create.system.ModuleProperty
	 * @property {String} Name	Property name
	 * @property {String} Value	Property value
	 * @see mzero.create.system.ModuleInformation
	 */
	function ModuleProperty(name/*String*/, value/*String*/) 
	{
		this.Name = name;
		this.Value = value;
	}
	
	/**
	 * Digested information about platform modules, which comprises the standard properties and any custom properties reported by the module
	 *
	 * @class mzero.create.system.ModuleInformation
	 * @property {String} MAPName Platform name
	 * @property {String} State Module status
	 * @property {String} Version Module version
	 * @property {Array} Others Array of {@link mzero.create.system.ModuleProperty}
	 * @see mzero.create.system.SystemReportResult
	 */
	function ModuleInformation(mapDeviceName/*String*/)
	{
		this.MAPName/*String*/ = mapDeviceName;
		this.State/*String*/ = null;
		this.Version/*String*/ = null;
		this.Others/*Array*//*ModuleProperty*/ = [];
	}
	ModuleInformation.prototype.useProperty = function(name/*String*/, value/*String*/)/*void*/
	{
		switch (name) 
		{
			case "Version":
				this.Version = value;
				break;
			
			case "State":
				this.State = value;
				break;
			
			default:
			{
				var property/*ModuleProperty*/ = new ModuleProperty(name, value);
				this.Others.push(property);
			}
		}
	}
	
	function processAndStoreModuleProperty(deviceMappings/*Object*/, deviceName/*String*/, propertyName/*String*/, value/*String*/)/*void*/ 
	{
		var info/*ModuleInformation*/ = getOrCreateModuleInformation(deviceMappings, deviceName);
		info.useProperty(propertyName, value);
	}
	
	function getOrCreateModuleInformation(deviceMappings/*Object*/, deviceName/*String*/)/*ModuleInformation*/
	{
		var info/*ModuleInformation*/ = deviceMappings[deviceName];
		if (info == null) {
			info = new ModuleInformation(deviceName);
			deviceMappings[deviceName] = info;
		}
		return info;
	}

	function sortModules(mappings)/*Array*/ {
		var result = [];
		Object.getOwnPropertyNames(mappings).sort().forEach(function(element, index, array) {
			result.push(mappings[element]);
		});
		return result;
	}

	var regexDevice = /^Device\.([A-Za-z_0-9\.]+)\.([A-Za-z_0-9]+)$/
	var regexService = /^Service\.([A-Za-z_0-9\.]+)\.([A-Za-z_0-9]+)$/
	
	function prepareReportResult(data) {
		if(!data) return null;
		var moduleEntries = data["Report"];
		if(!moduleEntries) return null;

		var deviceMappings/*Object*/ = { };
		var devices/*Array*//*ModuleInformation*/ = [];
		var serviceMappings/*Object*/ = { };
		var services/*Array*//*ModuleInformation*/ = [];
		var mapVersion/*String*/;
		
		moduleEntries.forEach(function(elem, index, array) {
			var key/*String*/ = elem["Key"];
			var value/*String*/ = elem["Value"];
			var match/*Object*/;
				
			if (key == "Map.Service.Version" ) {
				mapVersion = value;
			} else if((match = regexService.exec(key)) != null) {
				processAndStoreModuleProperty(serviceMappings, match[1], match[2], value);
			} else if ((match = regexDevice.exec(key)) != null) {
				processAndStoreModuleProperty(deviceMappings, match[1], match[2], value);
			}
		});
		/**
		 * Data obtained on successful completion of {@link mzero.create.system.SystemReportRequest}, which
		 * contains organized information about the platform status
		 *
		 * @class mzero.create.system.SystemReportResult
		 * @property {string} MAPVersion The platform version
		 * @property {Array} Devices Array of <tt>ModuleInformation</tt>
		 * @property {Array} Services Array of <tt>ModuleInformation</tt>
		 * @property {Object} DevicesMapping Mapping from device names to {@link mzero.create.system.ModuleInformation}
		 * @property {Object} ServicesMapping Mapping from service names to {@link mzero.create.system.ModuleInformation}
		 * @see mzero.create.system.SystemReportRequest
		 */
		var result/*DigestedSystemReport*/ = { };
		result.MAPVersion = mapVersion;
		result.DevicesMapping = deviceMappings;
		result.ServicesMapping = serviceMappings;
		result.Devices = sortModules(deviceMappings);
		result.Services = sortModules(serviceMappings);
		return result;
	};
	
	var ReasonNotSpecified = "Not specified";
	
	/** @lends mzero.create.system */
	var exports = {
		/**
		 * Represents a Request to obtain the system status
		 * @name mzero.create.system.SystemReportRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.system.report
		 * @see mzero.create.system.SystemReportResult
		 */
		/**
		 * @return {mzero.create.system.SystemReportRequest} A System report request ready to be started
		 * @see mzero.create.system.SystemReportResult
		 */
		report: function() {
			var self = this;
			var request = service.createRequest("SystemReport")
				.succeedsOn("SystemReportSucceeded")
				.transformResultUsing(prepareReportResult)
			;
			return request;
		},
		/**
		 * Represents a request to modify the system life cycle. The request will be submitted once
		 * <code>start()</code> is called.
		 *
		 * <p>
		 * When using life cycle requests, you need to consider:
		 * <ul>
		 * <li>Once a life cycle change has been initiated successfully (that is, when your <code>Done</code>
		 * callback is invoked with a null error code), your application does not receive any further progress
		 * notifications as the change is completed.</li>
		 * <li>The platform will take some time executing the life cycle change (usually no more than 30 seconds),
		 * therefore your application should change its UI state to match: the user will need to wait until the
		 * process completes.
		 * </li>
		 * </ul>
		 * </p>
		 *
		 * @name mzero.create.system.SystemLifeCycleChangeRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see {@link mzero.create.system.shutdown}
		 * @see {@link mzero.create.system.restart}
		 * @see {@link mzero.create.system.exitToDesktop}
		 * @see {@link mzero.create.system.customExit}
		 */
		/**
		 * Attempts to initiate a system shutdown.
		 * 
		 * @function shutdown
		 * @memberof mzero.create.system
		 * @static
		 *
		 * @param {string} reason - A descriptive shutdown reason
		 * @return {mzero.create.system.SystemLifeCycleChangeRequest} A life cycle change request ready to be started
		 */
		shutdown: function(reason) {
			var self = this;
			if(reason == null) reason = ReasonNotSpecified;
			var request = service.createRequest("SystemLifeCycleChange", { "Kind": "Shutdown", "CustomCode": 0, "Reason": reason })
				.succeedsOn("SystemLifeCycleChangeSucceeded")
			;
			return request;
		},
		/**
		 * Attempts to initiate a system restart. 
		 *
		 * @function restart
		 * @memberof mzero.create.system
		 * @static
		 *
		 * @param {string} reason - A descriptive restart reason
		 * @return {mzero.create.system.SystemLifeCycleChangeRequest} A life cycle change request ready to be started
		 */
		restart: function(reason) {
			var self = this;
			if(reason == null) reason = ReasonNotSpecified;
			var request = service.createRequest("SystemLifeCycleChange", { "Kind": "Restart", "CustomCode": 0, "Reason": reason })
				.succeedsOn("SystemLifeCycleChangeSucceeded")
			;
			return request;
		},
		/**
		 * Attempts to initiate an exit to system desktop
		 *
		 * @function exitToDesktop
		 * @memberof mzero.create.system
		 * @static
		 *
		 * @param {string} reason - A descriptive exit to desktop reason
		 * @return {mzero.create.system.SystemLifeCycleChangeRequest} A life cycle change request ready to be started
		 */
		exitToDesktop: function(reason) {
			var self = this;
			if(reason == null) reason = ReasonNotSpecified;
			var request = service.createRequest("SystemLifeCycleChange", { "Kind": "ExitToDesktop", "CustomCode": 0 , "Reason": reason})
				.succeedsOn("SystemLifeCycleChangeSucceeded")
			;
			return request;
		},
		/**
		 * Attempts to initiate a custom exit in which the platform closes to execute the custom steps identified by customCode.
		 *
		 * @function customExit
		 * @memberof mzero.create.system
		 * @static
		 *
		 * @param {int} customCode - A custom exit code configured in the system
		 * @param {string} reason - A descriptive exit reason
		 * @return {mzero.create.system.SystemLifeCycleChangeRequest} A life cycle change request ready to be started
		 */
		customExit: function(customCode, reason) {
			var self = this;
			if(reason == null) reason = ReasonNotSpecified;
			var request = service.createRequest("SystemLifeCycleChange", { "Kind": "CustomExit", "CustomCode" : customCode, "Reason": reason })
				.succeedsOn("SystemLifeCycleChangeSucceeded")
			;
			return request;
		}
	};
	return exports;
})();
