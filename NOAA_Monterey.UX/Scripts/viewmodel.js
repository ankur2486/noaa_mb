﻿var win;
var ViewModel = function () {

    var self = this;
    self.devMode = ko.observable(false);// set developer mode
    var mainDisplayTracker = null;
    self.pageTimerHandler = null;
    self.areYouThereTimerHandler = null;
    self.errorTimerHandler = null;
    self.defaultLanguageText = ko.observable();
    self.languageSpecification = ko.observable();
    self.pageName = ko.observable();
    self.pagesTimeOut = ko.observable();
    self.htmlContents = ko.observable();
    self.WeatherUrlText = ko.observable();
    self.commanUrlText = ko.observable();
    self.pageTiming = ko.observable();
    self.popupTiming = ko.observable();

    //General    
   
    self.previousPageName = ko.observable();
    self.mapServiceActivated = ko.observable(false);
    self.screenMessage = ko.observable();

    //nature
    self.wildName = ko.observable();
    self.wildSubName = ko.observable();
    self.wildDesc = ko.observable();
    self.wildImagePath = ko.observable();
    self.masterwildImagePath = ko.observable();
    self.fisheriesPagesText = ko.observable();

    //photo gallery
    self.photoHeading = ko.observable();
    self.photoSubHeading = ko.observable();
    self.photoDesc = ko.observable();
    self.photoImg = ko.observable();
    self.photoCount = ko.observable(0);
    self.isShowNextButton = ko.observable(false);

    //Video Gallery
    self.selectedVideo = ko.observable('');
    self.videoVal = ko.observable();

    //Weather
    self.weatherCurrentConditionSubHeading = ko.observable();
    self.weatherCurrentConditionDesc = ko.observable();
    self.weatherCurrentConditionImg1 = ko.observable();
    self.weatherCurrentConditionImg2 = ko.observable();
    self.weatherCurrentConditionImg3 = ko.observable();
    self.weatherCurrentConditionImg4 = ko.observable();
    self.weatherCurrentConditionTxt1 = ko.observable();
    self.weatherCurrentConditionTxt2 = ko.observable();
    self.weatherCurrentConditionTxt3 = ko.observable();
    self.weatherCurrentConditionTxt4 = ko.observable();
    self.WeatherUrl1 = ko.observable();
    self.WeatherUrl2 = ko.observable();
    self.WeatherUrl3 = ko.observable();
    self.WeatherUrl4 = ko.observable();

    //Endangered Species act
    self.mainHeading = ko.observable();
    self.subHeading = ko.observable();
    self.description = ko.observable();
    self.imagePath = ko.observable();
    self.whereText = ko.observable();
    self.costText = ko.observable();
    self.hourText = ko.observable();
    self.sanctuaryVisitorCenCount = ko.observable(0);

    self.selectedExternalLink = ko.observable();

    //About your Sanctuary
    self.leftMainHeading = ko.observable();
    self.rightMainHeading = ko.observable();
    self.leftSubHeading = ko.observable();
    self.rightSubHeading = ko.observable();
    self.subMainHeading = ko.observable();
    self.abtDescription = ko.observable();
    self.abtImage = ko.observable();
    self.checkAbtData = ko.observable(false);
    self.searchUrlArray = ko.observableArray();
    self.mainBtnFlag = ko.observable(false);

    var ser = new service(function (error) {
        console.log("Error invoking WCF method.");
        self.htmlContents("Error invoking WCF method.Please check service is running or not.");
        console.log(error);
        self.CallInitialiseAfterInterval();
    });

    self.setPage = function (page, callback) {
       
        self.previousPageName('');
        self.photoCount(0);
        self.setPhotoGalleryPageContent();
        self.sanctuaryVisitorCenCount(0);
        self.isShowNextButton(false);
        self.setSanctuaryVisitorCenPageContent();
        self.selectedVideo("");
        self.checkAbtData(false);

        ser.renderHTMLContents(page, function (response) {

            if (response.HTMLContents == "") {
                self.htmlContents("Please check template folder path in host service config file.");
                self.CallInitialiseAfterInterval();
                return;
            }

            
            if (self.pageTimerHandler)
                clearTimeout(self.pageTimerHandler);

            if (self.areYouThereTimerHandler)
                clearTimeout(self.areYouThereTimerHandler);
            if (page !== "Attract") {
                self.pageTiming(self.pagesTimeOut().monterey_pagetimeout.pageInactivity.text);
                self.popupTiming(self.pagesTimeOut().monterey_pagetimeout.areYouTherePopup.text);
                self.mainBtnFlag(false);
                self.addPageTimer();

                var video = document.getElementById("introAtractVideo");
                if (video) {
                    video.pause();
                    video.currentTime = 0
                }
            }

            self.htmlContents(response.HTMLContents);
            self.pageName(page);

            if (page == "Attract") {
                var video = document.getElementById("introAtractVideo");
                if (video) {
                    video.play();
                }
            }
            if(page =="YourNationalMaSanctOverview")
            {
                var video = document.getElementById("videoGallery");
                if (video) {
                    video.currentTime = 0
                    video.play();
                }
                
            }
            if (page == "OverviewMonterey")
            {
                var video = document.getElementById("videoGallery");
                if (video) {
                    video.currentTime = 0
                    video.play();
                }
            }

            if (callback && typeof callback == "function")
                callback();
        });
        if (self.screenMessage() != 'Error initializing MapService' && page != "WeatherCurrentCondition") {
            self.reportScreen(page, self.screenMessage());
        }
    }

    self.CallInitialiseAfterInterval = function () {
        self.errorTimerHandler = setTimeout(function () {
            self.initialize();
        }, 30000);
    }

    self.addPageTimer = function () {

        if (self.pageTimerHandler)
            clearTimeout(self.pageTimerHandler);
        if (self.areYouThereTimerHandler)
            clearTimeout(self.areYouThereTimerHandler);

        $("#areYouTherePopup").modal("hide");
        self.areYouThereTimerHandler = setTimeout(function () {

            $(".popover.fade.bottom.in").popover('hide');

            var video = $('video');
            var videoElement = video.get(0);

            if (videoElement && !videoElement.ended) {

                self.addPageTimer();

                return;
            }
            if (win != null) {
                win.close();
                win = null;
            }
            self.showPopup("#areYouTherePopup",
                               parseInt(self.pagesTimeOut().monterey_pagetimeout.areYouTherePopupTimeout.text));
        }, self.popupTiming());

        self.pageTimerHandler = setTimeout(function () {
            if (self.previousPageName() != '') {
                self.clearSession("Attract", "Session time out on " + self.previousPageName() + " page");
            }
            else {
                self.clearSession("Attract", "Session time out on " + self.pageName() + " page");
            }
        }, self.pageTiming());
    };

    self.showPopup = function (popupName, time) {
        $(popupName).modal('show');
        setTimeout(function () {
            $(popupName).modal("hide");
        }, time);

    };

    self.initialize = function () {
        if (self.devMode()) {
            self.getConfiguration();
            return;
        }

        var appData = { // optional application data
            MachineId1: "",
            SoftwareId: ""
        };
        var options = {
            Logging: null,
            Connection: {
                //Url: "ws://10.1.1.190:61623",
                Url: "ws://127.0.0.1:61623",
                Login: "mapkiosk",
                Passcode: "oY8tKFDPCQG4KG"
            },
            AppData: appData

        };

        var eventHandlers = {

            Done: function (err, data) {
                if (err) {
                    self.htmlContents("Error initializing MapService:" + err);
                    console.log("Error initializing MapService:" + err);
                    self.screenMessage('Error initializing MapService');
                    self.CallInitialiseAfterInterval();
                }
                else {
                    console.log("MapService Initialized successfully");
                    self.mapServiceActivated(true);

                    self.screenMessage("After Initialization");
                    self.getConfiguration();

                }
            }
        };

        if (!mzero.create.MAPEnv.isInitialized()) {
            mzero.create.MAPEnv.initialize(options, eventHandlers);
        }
        else {
            console.log("MapService already activated.");
            self.mapServiceActivated(true);

            self.screenMessage("After Initialization");

            self.getConfiguration();
        }
    };

    self.getConfiguration = function () {

        console.log("Version: 1.0.1");

        ser.getConfiguration(function (response) {
            self.errorTimerHandler = null;
            self.defaultLanguageText(JSON.parse(response.PagesText));
            self.languageSpecification(self.defaultLanguageText().monterey_screentext.language);

            self.WeatherUrlText(JSON.parse(response.WeatherDetails));
            self.commanUrlText(self.WeatherUrlText().noaa_montereyUrl.commanUrls);

            if (response.WeatherConfig == 1)
                self.WeatherUrlText(self.WeatherUrlText().noaa_montereyUrl.WeatherConfig1);
            else
                self.WeatherUrlText(self.WeatherUrlText().noaa_montereyUrl.WeatherConfig2);

            self.pagesTimeOut(JSON.parse(response.PagesTimeout));
            self.pageTiming(self.pagesTimeOut().monterey_pagetimeout.pageInactivity.text);
            self.popupTiming(self.pagesTimeOut().monterey_pagetimeout.areYouTherePopup.text);
            self.setPhotoGalleryPageContent();
            self.searchUrlArray(response.SearchURL);

            if (self.devMode()) {
                self.screenMessage('After Initialization');
                self.setPage("Attract");
                return;
            }

            self.enterSessionScreen('Attract', 'User clicked on start page to start session');
            self.screenMessage('After Initialization');
            self.setPage("Attract");

        });
    }

    self.getScreenTrackerSingleton = function () {
        if (mainDisplayTracker)
            return mainDisplayTracker;

        var options = {
            display: "main"
        };

        mainDisplayTracker = mzero.create.components.screentracker.create(options);
        return mainDisplayTracker;
    };

    self.reportScreen = function (screenName, reason) {
        if (self.devMode()) {
            console.log(screenName.toLowerCase() + " " + reason.toLowerCase());
            return;
        }
        self.getScreenTrackerSingleton().enter(screenName.toLowerCase(), reason.toLowerCase());
    };

    self.enterSessionScreen = function (screenName, reason) {

        if (self.devMode()) {
            console.log(screenName.toLowerCase() + " " + reason.toLowerCase());
            return;
        }

        self.getScreenTrackerSingleton().enterSession("Session_" + Date.now(), screenName.toLowerCase(), reason.toLowerCase());
    };

    self.exitSessionScreen = function (screenName, reason) {
        if (self.devMode()) {
            console.log(screenName.toLowerCase() + " " + reason.toLowerCase());
            return;
        }

        self.getScreenTrackerSingleton().exitSession(screenName.toLowerCase(), reason.toLowerCase());
    };

    self.commanSetPagefun = function (page, msg) {
        self.screenMessage(msg);
        self.setPage(page);
    }

    self.clearSession = function (page, message) {
        self.closeExternalLinkPopup();
        self.mainBtnFlag(false);
        self.exitSessionScreen(page, message);
        self.screenMessage(message);
        self.commanSetPagefun(page, "Start new session.");
    };

    self.getSelectedWildDetails = function (type, value) {

        self.addPageTimer();
        if (self.pageName() != "NatureDetails") {
            self.screenMessage("User click on " + type + " on " + self.pageName());
            self.setPage("NatureDetails");
        }

        switch (type) {
            case self.languageSpecification().natureDetailsPage.CaliforniaSeaLionPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.CaliforniaSeaLionPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.CaliforniaSeaLionPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.CaliforniaSeaLionPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.CaliforniaSeaLionPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.SouthernSeaOtterPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.SouthernSeaOtterPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.SouthernSeaOtterPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.SouthernSeaOtterPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.SouthernSeaOtterPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.HarborSealPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.HarborSealPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.HarborSealPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.HarborSealPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.HarborSealPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.HumpbackWhalePage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.HumpbackWhalePage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.HumpbackWhalePage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.HumpbackWhalePage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.HumpbackWhalePage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.GrayWhalePage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.GrayWhalePage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.GrayWhalePage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.GrayWhalePage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.GrayWhalePage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.KillerWhalePage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.KillerWhalePage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.KillerWhalePage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.KillerWhalePage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.KillerWhalePage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.ElephantSealPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.ElephantSealPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.ElephantSealPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.ElephantSealPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.ElephantSealPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.GiantKelpPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.GiantKelpPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.GiantKelpPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.GiantKelpPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.GiantKelpPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.BrownPelicanPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.BrownPelicanPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.BrownPelicanPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.BrownPelicanPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.BrownPelicanPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.WesternGullPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.WesternGullPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.WesternGullPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.WesternGullPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.WesternGullPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.SootyShearwaterPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.SootyShearwaterPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.SootyShearwaterPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.SootyShearwaterPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.SootyShearwaterPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.LeatherbackTurtlePage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.LeatherbackTurtlePage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.LeatherbackTurtlePage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.LeatherbackTurtlePage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.LeatherbackTurtlePage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.MarketSquidPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.MarketSquidPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.MarketSquidPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.MarketSquidPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.MarketSquidPage.imagePath1Key);
                break;

            case self.languageSpecification().natureDetailsPage.NorthernAnchovyPage.nameKey:
                self.wildName(self.languageSpecification().natureDetailsPage.NorthernAnchovyPage.nameKey);
                self.wildSubName(self.languageSpecification().natureDetailsPage.NorthernAnchovyPage.subnameKey);
                self.wildDesc(self.languageSpecification().natureDetailsPage.NorthernAnchovyPage.descKey);
                self.masterwildImagePath(self.languageSpecification().natureDetailsPage.NorthernAnchovyPage.imagePath1Key);
                break;
        }

    }

    self.setPhotoGalleryPageContent = function () {
     
        self.addPageTimer();
        self.photoCount(self.photoCount() + 1);

        switch (self.photoCount()) {
            case 1:
                self.photoHeading("");
                self.photoSubHeading("");
                self.photoDesc(self.languageSpecification().photoGalleryPage.welcomePage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.welcomePage.imgKey);
                break;

            case 2:
                self.photoHeading(self.languageSpecification().photoGalleryPage.californiaseaotterPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.californiaseaotterPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.californiaseaotterPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.californiaseaotterPage.imgKey);
                break;
            case 3:
                self.photoHeading(self.languageSpecification().photoGalleryPage.naturalbridgesPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.naturalbridgesPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.naturalbridgesPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.naturalbridgesPage.imgKey);
                break;
            case 4:
                self.photoHeading(self.languageSpecification().photoGalleryPage.strawberryanemonePage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.strawberryanemonePage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.strawberryanemonePage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.strawberryanemonePage.imgKey);
                break;
            case 5:
                self.photoHeading(self.languageSpecification().photoGalleryPage.vermillionrockfishPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.vermillionrockfishPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.vermillionrockfishPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.vermillionrockfishPage.imgKey);
                break;
            case 6:
                self.photoHeading(self.languageSpecification().photoGalleryPage.redabalonePage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.redabalonePage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.redabalonePage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.redabalonePage.imgKey);
                break;
                //remaining
            case 7:
                self.photoHeading(self.languageSpecification().photoGalleryPage.greategretPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.greategretPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.greategretPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.greategretPage.imgKey);
                break;
            case 8:
                self.photoHeading(self.languageSpecification().photoGalleryPage.ocherstarPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.ocherstarPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.ocherstarPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.ocherstarPage.imgKey);
                break;
            case 9:
                self.photoHeading(self.languageSpecification().photoGalleryPage.melibePage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.melibePage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.melibePage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.melibePage.imgKey);
                break;
            case 10:
                self.photoHeading(self.languageSpecification().photoGalleryPage.bugSurCoastPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.bugSurCoastPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.bugSurCoastPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.bugSurCoastPage.imgKey);
                break;
            case 11:
                self.photoHeading(self.languageSpecification().photoGalleryPage.seapalmPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.seapalmPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.seapalmPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.seapalmPage.imgKey);
                break;
            case 12:
                self.photoHeading(self.languageSpecification().photoGalleryPage.opalescentnudibranchPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.opalescentnudibranchPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.opalescentnudibranchPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.opalescentnudibranchPage.imgKey);
                break;
            case 13:
                self.photoHeading(self.languageSpecification().photoGalleryPage.greenseaanemonePage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.greenseaanemonePage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.greenseaanemonePage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.greenseaanemonePage.imgKey);
                break;
            case 14:
                self.photoHeading(self.languageSpecification().photoGalleryPage.jeweledtopsnailPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.jeweledtopsnailPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.jeweledtopsnailPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.jeweledtopsnailPage.imgKey);
                break;
            case 15:
                self.photoHeading(self.languageSpecification().photoGalleryPage.scubadivingPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.scubadivingPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.scubadivingPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.scubadivingPage.imgKey);
                break;
            case 16:
                self.photoHeading(self.languageSpecification().photoGalleryPage.californiahydrocoralPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.californiahydrocoralPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.californiahydrocoralPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.californiahydrocoralPage.imgKey);
                break;
            case 17:
                self.photoHeading(self.languageSpecification().photoGalleryPage.batstarPage.headingKey);
                self.photoSubHeading(self.languageSpecification().photoGalleryPage.batstarPage.subHeadingKey);
                self.photoDesc(self.languageSpecification().photoGalleryPage.batstarPage.descKey);
                self.photoImg(self.languageSpecification().photoGalleryPage.batstarPage.imgKey);
                self.isShowNextButton(true);
                break;

        }
    };

    self.setSanctuaryVisitorCenPageContent = function () {
        self.addPageTimer();
        self.sanctuaryVisitorCenCount(self.sanctuaryVisitorCenCount() + 1);

        switch (self.sanctuaryVisitorCenCount()) {
            case 1:
                self.mainHeading(self.languageSpecification().sanctuaryVisitorCenPage.sanctuaryExpCenPage.titleKey);
                self.description(self.languageSpecification().sanctuaryVisitorCenPage.sanctuaryExpCenPage.descriptionKey);
                self.imagePath(self.languageSpecification().sanctuaryVisitorCenPage.sanctuaryExpCenPage.imgKey);
                self.whereText(self.languageSpecification().sanctuaryVisitorCenPage.sanctuaryExpCenPage.whereKey);
                self.costText(self.languageSpecification().sanctuaryVisitorCenPage.sanctuaryExpCenPage.hourKey);
                self.hourText(self.languageSpecification().sanctuaryVisitorCenPage.sanctuaryExpCenPage.costKey);
                break;

            case 2:
                self.mainHeading(self.languageSpecification().sanctuaryVisitorCenPage.coastalDiscoveryCenPage.titleKey);
                self.description(self.languageSpecification().sanctuaryVisitorCenPage.coastalDiscoveryCenPage.descriptionKey);
                self.imagePath(self.languageSpecification().sanctuaryVisitorCenPage.coastalDiscoveryCenPage.imgKey);
                self.whereText(self.languageSpecification().sanctuaryVisitorCenPage.coastalDiscoveryCenPage.whereKey);
                self.costText(self.languageSpecification().sanctuaryVisitorCenPage.coastalDiscoveryCenPage.hourKey);
                self.hourText(self.languageSpecification().sanctuaryVisitorCenPage.coastalDiscoveryCenPage.costKey);
                self.isShowNextButton(true);
                break;
        }
    };

    self.playSelectedVideo = function (videoPath, videoVal) {
        self.selectedVideo('');
        self.selectedVideo(videoPath);
        self.videoVal(videoVal);


        var video = document.getElementById("videoGallery");
        if (video) {
            video.pause();
            video.currentTime = 0

            setTimeout(function () { video.play(); }, 150)
        }
    };

    self.WeatherCallbackFun = function (type) {

        var control = $(".rightButtonsBorder").find(".rightBtnSelectedText");
        $(control).removeClass("rightBtnSelectedText");

        var control13 = $(".rightButtonsBorder").find(".disabledCSS");
        $(control13).removeClass("disabledCSS");

        var id = $('.rightButtonsBorder').find(":button");
        for (var i = 0; i < id.length; i++) {
            $(id)[i].disabled = false;
        }

        var control1 = $(".rightButtonsBorder").find(".selectedBtnIcon");
        $(control1).removeClass("selectedBtnIcon");

        switch (type) {
            case self.languageSpecification().rightSideButtons.currentConditionBtnKey:
                var control = $("#divCurrentCondition").find(".rightBtnBackground");
                $(control).addClass("rightBtnSelectedText");
                var id = $('#divCurrentCondition').find("#btnCurrentCondition");
                $(id)[0].disabled = true;
                var control1 = $("#divCurrentCondition").find(".normalIcon");
                $(control1).addClass("selectedBtnIcon");
                $('#btnCurrentCondition').addClass("disabledCSS");
                self.weatherCurrentConditionSubHeading(self.languageSpecification().rightSideButtons.currentConditionBtnKey);
                self.weatherCurrentConditionDesc(self.languageSpecification().weatherCurrentConditionPage.discriptionKey);
                self.weatherCurrentConditionImg1(self.languageSpecification().weatherCurrentConditionPage.bottomImg1);
                self.weatherCurrentConditionImg2(self.languageSpecification().weatherCurrentConditionPage.bottomImg2);
                self.weatherCurrentConditionImg3(self.languageSpecification().weatherCurrentConditionPage.bottomImg3);
                self.weatherCurrentConditionImg4(self.languageSpecification().weatherCurrentConditionPage.bottomImg4);
                self.weatherCurrentConditionTxt1(self.languageSpecification().weatherCurrentConditionPage.bottomtext1);
                self.weatherCurrentConditionTxt2(self.languageSpecification().weatherCurrentConditionPage.bottomtext2);
                self.weatherCurrentConditionTxt3(self.languageSpecification().weatherCurrentConditionPage.bottomtext3);
                self.weatherCurrentConditionTxt4(self.languageSpecification().weatherCurrentConditionPage.bottomtext4);
                self.WeatherUrl1(self.WeatherUrlText().currentCondition.localWeather);
                self.WeatherUrl2(self.WeatherUrlText().currentCondition.hourlyAreaWeather);
                self.WeatherUrl3(self.WeatherUrlText().currentCondition.buoyInformation);
                self.WeatherUrl4(self.WeatherUrlText().currentCondition.sst);
                break;

            case self.languageSpecification().rightSideButtons.forecastsAndwarningsBtnKey:
                var control = $("#divForcastWarning").find(".rightBtnBackground");
                $(control).addClass("rightBtnSelectedText");
                var id = $('#divForcastWarning').find("#btnForcastWarning");
                $(id)[0].disabled = true;
                var control1 = $("#divForcastWarning").find(".normalIcon");
                $(control1).addClass("selectedBtnIcon");
                $('#btnForcastWarning').addClass("disabledCSS");
                self.weatherCurrentConditionSubHeading(self.languageSpecification().comman.forecastWarningKey);
                self.weatherCurrentConditionDesc(self.languageSpecification().forecastsAndwarningsPage.discriptionKey);
                self.weatherCurrentConditionImg1(self.languageSpecification().forecastsAndwarningsPage.bottomImg1);
                self.weatherCurrentConditionImg2(self.languageSpecification().forecastsAndwarningsPage.bottomImg2);
                self.weatherCurrentConditionImg3(self.languageSpecification().forecastsAndwarningsPage.bottomImg3);
                self.weatherCurrentConditionImg4(self.languageSpecification().forecastsAndwarningsPage.bottomImg4);
                self.weatherCurrentConditionTxt1(self.languageSpecification().forecastsAndwarningsPage.bottomtext1);
                self.weatherCurrentConditionTxt2(self.languageSpecification().forecastsAndwarningsPage.bottomtext2);
                self.weatherCurrentConditionTxt3(self.languageSpecification().forecastsAndwarningsPage.bottomtext3);
                self.weatherCurrentConditionTxt4(self.languageSpecification().forecastsAndwarningsPage.bottomtext4);
                self.WeatherUrl1(self.WeatherUrlText().forecastsandWarnings.graphicalForcasts);
                self.WeatherUrl2(self.WeatherUrlText().forecastsandWarnings.coastalWaters);
                self.WeatherUrl3(self.WeatherUrlText().forecastsandWarnings.farOffshoreWaters);
                self.WeatherUrl4(self.WeatherUrlText().forecastsandWarnings.marineandWeather);
                break;

            case self.languageSpecification().rightSideButtons.surfWindsTidesBtnKey:
                var control = $("#divsurfWindsTides").find(".rightBtnBackground");
                $(control).addClass("rightBtnSelectedText");
                var id = $('#divsurfWindsTides').find("#btnsurfWindsTides");
                $(id)[0].disabled = true;
                var control1 = $("#divsurfWindsTides").find(".normalIcon");
                $(control1).addClass("selectedBtnIcon");
                $('#btnsurfWindsTides').addClass("disabledCSS");
                self.weatherCurrentConditionSubHeading(self.languageSpecification().rightSideButtons.surfWindsTidesBtnKey);
                self.weatherCurrentConditionDesc(self.languageSpecification().surfWindsTidesPage.discriptionKey);
                self.weatherCurrentConditionImg1(self.languageSpecification().surfWindsTidesPage.bottomImg1);
                self.weatherCurrentConditionImg2(self.languageSpecification().surfWindsTidesPage.bottomImg2);
                self.weatherCurrentConditionImg3(self.languageSpecification().surfWindsTidesPage.bottomImg3);
                self.weatherCurrentConditionImg4(self.languageSpecification().surfWindsTidesPage.bottomImg4);
                self.weatherCurrentConditionTxt1(self.languageSpecification().surfWindsTidesPage.bottomtext1);
                self.weatherCurrentConditionTxt2(self.languageSpecification().surfWindsTidesPage.bottomtext2);
                self.weatherCurrentConditionTxt3(self.languageSpecification().surfWindsTidesPage.bottomtext3);
                self.weatherCurrentConditionTxt4(self.languageSpecification().surfWindsTidesPage.bottomtext4);
                self.WeatherUrl1(self.WeatherUrlText().surfWindsTides.monetoryTides);
                self.WeatherUrl2(self.WeatherUrlText().surfWindsTides.wind);
                self.WeatherUrl3(self.WeatherUrlText().surfWindsTides.surf);
                self.WeatherUrl4(self.WeatherUrlText().surfWindsTides.waveHeight);
                break;

            case self.languageSpecification().rightSideButtons.radarStateliteBtnKey:
                var control = $("#divRadar").find(".rightBtnBackground");
                $(control).addClass("rightBtnSelectedText");
                var id = $('#divRadar').find("#btnRadar");
                $(id)[0].disabled = true;
                var control1 = $("#divRadar").find(".normalIcon");
                $(control1).addClass("selectedBtnIcon");
                $('#btnRadar').addClass("disabledCSS");
                self.weatherCurrentConditionSubHeading(self.languageSpecification().rightSideButtons.radarStateliteBtnKey);
                self.weatherCurrentConditionDesc(self.languageSpecification().radarStatelitePage.discriptionKey);
                self.weatherCurrentConditionImg1(self.languageSpecification().radarStatelitePage.bottomImg1);
                self.weatherCurrentConditionImg2(self.languageSpecification().radarStatelitePage.bottomImg2);
                self.weatherCurrentConditionImg3(self.languageSpecification().radarStatelitePage.bottomImg3);
                self.weatherCurrentConditionImg4(self.languageSpecification().radarStatelitePage.bottomImg4);
                self.weatherCurrentConditionTxt1(self.languageSpecification().radarStatelitePage.bottomtext1);
                self.weatherCurrentConditionTxt2(self.languageSpecification().radarStatelitePage.bottomtext2);
                self.weatherCurrentConditionTxt3(self.languageSpecification().radarStatelitePage.bottomtext3);
                self.weatherCurrentConditionTxt4(self.languageSpecification().radarStatelitePage.bottomtext4);
                self.WeatherUrl1(self.WeatherUrlText().radarSatellite.pacificSW);
                self.WeatherUrl2(self.WeatherUrlText().radarSatellite.localArea);
                self.WeatherUrl3(self.WeatherUrlText().radarSatellite.visibleSatellite);
                self.WeatherUrl4(self.WeatherUrlText().radarSatellite.easternPacificInfrared);
                break;

            case self.languageSpecification().rightSideButtons.extremeWeatherBtnKey:
                var control = $("#divextremeWeather").find(".rightBtnBackground");
                $(control).addClass("rightBtnSelectedText");
                var id = $('#divextremeWeather').find("#btnextremeWeather");
                $(id)[0].disabled = true;
                var control1 = $("#divextremeWeather").find(".normalIcon");
                $(control1).addClass("selectedBtnIcon");
                $('#btnextremeWeather').addClass("disabledCSS");
                self.weatherCurrentConditionSubHeading(self.languageSpecification().extremeWeatherPage.discriptionTitleKey);
                self.weatherCurrentConditionDesc(self.languageSpecification().extremeWeatherPage.discriptionKey);
                self.weatherCurrentConditionImg1(self.languageSpecification().extremeWeatherPage.bottomImg1);
                self.weatherCurrentConditionImg2(self.languageSpecification().extremeWeatherPage.bottomImg2);
                self.weatherCurrentConditionImg3(self.languageSpecification().extremeWeatherPage.bottomImg3);
                self.weatherCurrentConditionImg4(self.languageSpecification().extremeWeatherPage.bottomImg4);
                self.weatherCurrentConditionTxt1(self.languageSpecification().extremeWeatherPage.bottomtext1);
                self.weatherCurrentConditionTxt2(self.languageSpecification().extremeWeatherPage.bottomtext2);
                self.weatherCurrentConditionTxt3(self.languageSpecification().extremeWeatherPage.bottomtext3);
                self.weatherCurrentConditionTxt4(self.languageSpecification().extremeWeatherPage.bottomtext4);
                self.WeatherUrl1(self.WeatherUrlText().extremeWeather.ripCurrents);
                self.WeatherUrl2(self.WeatherUrlText().extremeWeather.tsunamis);
                self.WeatherUrl3(self.WeatherUrlText().extremeWeather.atmosphericRivers);
                self.WeatherUrl4(self.WeatherUrlText().extremeWeather.lightning);
                break;

            case self.languageSpecification().rightSideButtons.climateChangeBtnKey:
                var control = $("#divclimateChange").find(".rightBtnBackground");
                $(control).addClass("rightBtnSelectedText");
                var id = $('#divclimateChange').find("#btnclimateChange");
                $(id)[0].disabled = true;
                var control1 = $("#divclimateChange").find(".normalIcon");
                $(control1).addClass("selectedBtnIcon");
                $('#btnclimateChange').addClass("disabledCSS");
                self.weatherCurrentConditionSubHeading(self.languageSpecification().climateChangePage.discriptionTitleKey);
                self.weatherCurrentConditionDesc(self.languageSpecification().climateChangePage.discriptionKey);
                self.weatherCurrentConditionImg1(self.languageSpecification().climateChangePage.bottomImg1);
                self.weatherCurrentConditionImg2(self.languageSpecification().climateChangePage.bottomImg2);
                self.weatherCurrentConditionImg3(self.languageSpecification().climateChangePage.bottomImg3);
                self.weatherCurrentConditionImg4(self.languageSpecification().climateChangePage.bottomImg4);
                self.weatherCurrentConditionTxt1(self.languageSpecification().climateChangePage.bottomtext1);
                self.weatherCurrentConditionTxt2(self.languageSpecification().climateChangePage.bottomtext2);
                self.weatherCurrentConditionTxt3(self.languageSpecification().climateChangePage.bottomtext3);
                self.weatherCurrentConditionTxt4(self.languageSpecification().climateChangePage.bottomtext4);
                self.WeatherUrl1(self.WeatherUrlText().climateChange.indicators);
                self.WeatherUrl2(self.WeatherUrlText().climateChange.usSeasonalDroughtOutlook);
                self.WeatherUrl3(self.WeatherUrlText().climateChange.variability);
                self.WeatherUrl4(self.WeatherUrlText().climateChange.elNiño);
                break;
        }
    };

    self.getSelectedWeatherPage = function (type, value) {

        self.addPageTimer();
        if (self.pageName() != "WeatherCurrentCondition") {
            self.reportScreen(type, "user click on " + type + " on " + self.pageName() + " page");
            self.setPage("WeatherCurrentCondition", function () {
                self.previousPageName(type);
                self.WeatherCallbackFun(type)
            });
        }
        else {
            self.reportScreen(type, "user click on " + type + " on " + self.previousPageName() + " page");
            self.previousPageName(type);
            self.WeatherCallbackFun(type);
        }
    }

    self.playExternalLink = function (linkPath) {


        self.pageTiming(self.pagesTimeOut().monterey_pagetimeout.weatherPopupInactivity.text);
        self.popupTiming(self.pagesTimeOut().monterey_pagetimeout.WeatherpopupTiming.text);
        self.addPageTimer();
        //self.selectedSwfFile(0);
        self.selectedVideo("");
        //self.selectedVideoType("");
        const qs = require("querystring");

        const BrowserWindow = require('electron').remote.BrowserWindow;

        if (win == null) {
            win = new BrowserWindow({
                width: 1000, height: 900, show: false, resizable: false, movable: false, minimizable: false, maximizable: false, closable: true, alwaysOnTop: false, skipTaskbar: true, title: "", fullscreen: false, icon: __dirname + '/Image/Logo.png',
                fullscreenable: false, autoHideMenuBar: false, type: 'textured', 'webPreferences': { 'plugins': true, 'nodeIntegration': false }
            });

            win.on('closed', function () {
                win = null;
            });

            win.on('minimize', function () {
                win.close();
                win = null;
            });
        }
        if (linkPath.indexOf('.pdf') > 0) {

            const param = qs.stringify({ file: linkPath });
            win.loadURL('file://' + __dirname + '/Plugins/pdfjs/web/viewer.html?' + param);
        }
        else {
            win.loadURL(linkPath);
        }

        var webContents = win.webContents;

        webContents.on("will-navigate", function (e, url) {

            var urlMatch = ko.utils.arrayFirst(self.searchUrlArray(), function (item) {
                var itemText = item.split("*").join("")
                return url.indexOf(itemText) > -1;
            });

            if (urlMatch == "") {
                e.preventDefault();
                webContents.loadURL(webContents.getURL());
            }
        });

        webContents.on("new-window", function (e, url, frameName, disposition, options) {

            var urlMatch = ko.utils.arrayFirst(self.searchUrlArray(), function (item) {
                var itemText = item.split("*").join("")
                return url.indexOf(itemText) > -1;
            });

            if (urlMatch == "" || url.indexOf("print_download=true") > -1) {
                webContents.loadURL(webContents.getURL());
                e.returnValue = false;
            }
            // win.loadURL(urlMatch);
        });

        win.show();

    };

    self.closeExternalLinkPopup = function () {
        self.selectedExternalLink('');
        $('#showExternalLink').modal('hide');
        self.pageTiming(self.pagesTimeOut().monterey_pagetimeout.pageInactivity.text);
        self.popupTiming(self.pagesTimeOut().monterey_pagetimeout.areYouTherePopup.text);
        self.addPageTimer();
    };

    $("#showExternalLink").on('hidden.bs.modal', function () {
        self.selectedExternalLink('');
        self.pageTiming(self.pagesTimeOut().monterey_pagetimeout.pageInactivity.text);
        self.addPageTimer();
    });

    self.setAboutYourSanPageContent = function (pageNo) {
        self.addPageTimer();
        self.selectedVideo('');
        self.checkAbtData(true);

        switch (pageNo) {
            case 1:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageOne.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageOne.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageOne.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageOne.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageOne.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageOne.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageOne.imageKey);
                break;
            case 2:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTwo.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTwo.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTwo.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTwo.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTwo.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageTwo.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageTwo.imageKey);
                break;
            case 3:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThree.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThree.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThree.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThree.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThree.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageThree.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageThree.imageKey);
                break;
            case 4:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFour.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFour.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFour.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFour.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFour.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageFour.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageFour.imageKey);
                break;
            case 5:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFive.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFive.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFive.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFive.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFive.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageFive.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageFive.imageKey);
                break;
            case 6:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSix.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSix.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSix.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSix.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSix.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageSix.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageSix.imageKey);
                break;
            case 7:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSeven.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSeven.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSeven.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSeven.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSeven.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageSeven.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageSeven.imageKey);
                break;
            case 8:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEight.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEight.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEight.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEight.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEight.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageEight.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageEight.imageKey);
                break;
            case 9:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageNine.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageNine.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageNine.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageNine.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageNine.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageNine.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageNine.imageKey);
                break;
            case 10:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTen.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTen.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTen.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTen.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageTen.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageTen.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageTen.imageKey);
                break;
            case 11:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEleven.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEleven.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEleven.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEleven.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageEleven.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageEleven.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageEleven.imageKey);
                break;
            case 12:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pagetwelve.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pagetwelve.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pagetwelve.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pagetwelve.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pagetwelve.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pagetwelve.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pagetwelve.imageKey);
                break;
            case 13:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThirteen.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThirteen.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThirteen.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThirteen.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageThirteen.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageThirteen.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageThirteen.imageKey);
                break;
            case 14:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFourteen.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFourteen.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFourteen.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFourteen.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFourteen.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageFourteen.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageFourteen.imageKey);
                break;
            case 15:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFifteen.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFifteen.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFifteen.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFifteen.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageFifteen.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageFifteen.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageFifteen.imageKey);
                break;
            case 16:
                self.leftMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSixteen.mainHeadLeftKey);
                self.rightMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSixteen.mainHeadRightKey);
                self.leftSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSixteen.leftsubHeadKey);
                self.rightSubHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSixteen.rightsubHeadKey);
                self.subMainHeading(self.languageSpecification().aboutYourSanctuaryContentPage.pageSixteen.subMainHeadingKey);
                self.abtDescription(self.languageSpecification().aboutYourSanctuaryContentPage.pageSixteen.descKey);
                self.abtImage(self.languageSpecification().aboutYourSanctuaryContentPage.pageSixteen.imageKey);
                break;
        }
    };

    self.closeAbtYourSanWindow = function () {
        self.addPageTimer();
        self.checkAbtData(false);
        self.selectedVideo("");
    };
    self.showMenuButton = function () {
        if (!self.mainBtnFlag()) {
            self.mainBtnFlag(true);
            self.enterSessionScreen('NavigationMenu', 'User start session');
        }
    }

    self.initialize();
};

var FisheriesModel = function (data) {
    var self = this;
    self.DiscriptionTitle = ko.observable(data.DiscriptionTitle),
    self.Discription = ko.observable(data.Discription),
    self.LeftImage = ko.observable(data.LeftImage),
    self.CenterTopImage = ko.observable(data.CenterTopImage),
    self.CenterBottomImage = ko.observable(data.CenterBottomImage),
    self.RightImage = ko.observable(data.RightImage);
};



